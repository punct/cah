const { PLAYER_CONNECTED } = require('./actions')

function reducer (prevState, action) {
  switch (action.type) {
    case PLAYER_CONNECTED:
      prevState.players.push(action.payload)
      return prevState
    default:
      return prevState
  }
}

module.exports = reducer
