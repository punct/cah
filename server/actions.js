const PLAYER_CONNECTED = 'PLAYER_CONNECTED'

function playerConnected (params) {
  return {
    type: PLAYER_CONNECTED,
    payload: params
  }
}

module.exports = {
  PLAYER_CONNECTED,
  playerConnected
}
