const WebSocketServer = require('ws').Server
const qs = require('querystring')
const { createStore } = require('redux')
const reducer = require('./reducer')
const { playerConnected } = require('./actions')

const PORT = process.env.PORT || 8080

const initialState = {
  players: [],
  parties: [],
  games: []
}

const store = createStore(
  reducer,
  initialState
)

store.subscribe(() => {
  console.log('new state', store.getState())
})

// ----------

const wss = new WebSocketServer({ port: PORT })

wss.on('connection', (ws, request) => {
  const params = qs.parse(request.url.replace('/?', ''))
  const { nickname } = params

  store.dispatch(playerConnected({ nickname }))

  ws.on('message', msg => {
    console.log('msg', msg)
  })
})

wss.on('listening', () => {
  console.log(`Listening on ${wss.options.port}`)
})
