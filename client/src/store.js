import { createStore } from 'redux'
import { AppReducer } from './reducers'

const questions = [
  { id: 1, label: 'For my next trick, I will pull ____ out of ____.', draw: 0, pick: 2 },
  { id: 2, label: 'What gets better with age?', draw: 0, pick: 1 },
  { id: 3, label: 'What is Batman\'s guily pleasure?', draw: 0, pick: 1 },
  { id: 4, label: 'What\'s that sound?', draw: 0, pick: 1 },
  { id: 5, label: 'Cancel all my meetings. We\'ve got a situation with ____ that requires my immediate attention.', draw: 0, pick: 1 }
]
const answers = [
  { id: 1, label: 'Winking at old people.' },
  { id: 2, label: 'A snapping turtle biting the tip of your penis.' },
  { id: 3, label: 'Poor life choices.' },
  { id: 4, label: 'My browsing history.' },
  { id: 5, label: 'Getting wed, having a few kids, taking some pictures, retiring to the south of France, and dying.' },
  { id: 6, label: 'Winking at old people.' },
  { id: 7, label: 'A snapping turtle biting the tip of your penis.' },
  { id: 8, label: 'Poor life choices.' },
  { id: 9, label: 'My browsing history.' },
  { id: 10, label: 'Getting wed, having a few kids, taking some pictures, retiring to the south of France, and dying.' }
]
const party = [
  {
    id: 'player1',
    avatar: 'https://image.flaticon.com/icons/png/128/236/236832.png',
    displayName: 'Player1'
  },
  {
    id: 'player2',
    avatar: 'https://image.flaticon.com/icons/png/128/188/188987.png',
    displayName: 'Player2'
  },
  {
    id: 'player3',
    avatar: 'https://image.flaticon.com/icons/png/128/290/290454.png',
    displayName: 'Player3'
  }
]
const currentUser = {
  id: 'punct',
  avatar: 'http://pm1.narvii.com/6038/b833f0b2c9ca0c51e679546d0ea2af436fbd6d6c_00.jpg',
  displayName: 'punct'
}

const initialState = {
  currentUser: null,
  gameStarted: false,
  party: party,
  currentQuestion: questions[0],
  currentHand: answers
}

const ws = new WebSocket('ws://localhost:8080/?nickname=punct')
ws.onopen = () => {
  console.log('channel opened')
}
ws.onmessage = (msg) => {
  console.log('msg', msg)
}

const reducer = AppReducer

export const store = createStore(
  reducer,
  initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
