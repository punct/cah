import { LOGIN_SUBMIT, SELECT_HAND_CARD, SUBMIT_ANSWERS, START_GAME } from './actions'

function AppReducer (prevState, action) {
  switch (action.type) {
    case LOGIN_SUBMIT:
      prevState.party.push({ avatar: '', nickname: action.payload })
      return {
        ...prevState,
        currentUser: {
          avatar: '',
          nickname: action.payload
        }
      }
    case SELECT_HAND_CARD:
      const currentHand = prevState.currentHand.slice()
      for (let i = 0; i < currentHand.length; i++) {
        if (currentHand[i].id === action.payload.id) {
          currentHand[i] = {
            ...currentHand[i],
            selected: !currentHand[i].selected
          }
        }
      }
      return {
        ...prevState,
        currentHand
      }

    case SUBMIT_ANSWERS:
      console.log('@TODO: Submit answers', action)
      return prevState

    case START_GAME:
      return {
        ...prevState,
        gameStarted: true
      }

    default:
      return prevState
  }
}

export {
  AppReducer
}
