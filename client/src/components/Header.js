import { createElement } from 'react'
import { connect } from 'react-redux'
import { div, header, img } from './utils'

function HeaderUser (props) {
  const { user } = props
  return (
    div({ className: 'player' }, [
      (user)
        ? img({ className: 'player-avatar', src: user.avatar, alt: user.nickname })
        : null,
      div({ className: 'player-details' })
    ])
  )
}

function renderLogo () {
  return (
    div({ className: 'logo' }, 'Cards Against Humanity')
  )
}

function renderParty (players) {
  return (
    div({ className: 'party' },
      players.map(player => createElement(HeaderUser, {
        user: player
      }))
    )
  )
}

function renderCurrentUser (user) {
  return (
    div({ className: 'current-user' }, [
      createElement(HeaderUser, {
        user
      })
    ])
  )
}

function Header (props) {
  return (
    header({}, [
      renderLogo(),
      renderParty(props.party),
      renderCurrentUser(props.currentUser)
    ])
  )
}

function mapStateToProps (state) {
  return {
    currentUser: state.currentUser,
    party: state.party
  }
}

function mapDispatchToProps (dispatch) {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header)
