import { createElement } from 'react'
import { connect } from 'react-redux'
import { div } from './utils'

import Header from './Header'
import CurrentQuestion from './CurrentQuestion'
import CurrentHand from './CurrentHand'
import Lobby from './Lobby'
import Login from './Login'

function Game () {
  return (
    div({ className: 'game' }, [
      createElement(CurrentQuestion),
      createElement(CurrentHand)
    ])
  )
}

function App (props) {
  const { currentUser, gameStarted } = props

  return (
    div({ className: 'app' }, [
      (gameStarted)
        ? createElement(Game)
        : createElement(Lobby)
      // createElement(Header),
      // (!currentUser)
      //   ? createElement(Login)
      //   : (gameStarted)
      //     ? createElement(Game)
      //     : createElement(Lobby)
    ])
  )
}

function mapStateToProps (state) {
  return {
    currentUser: state.currentUser,
    gameStarted: state.gameStarted
  }
}

function mapDispatchToProps (dispatch) {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
