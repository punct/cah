import h from 'react-hyperscript'
import hh from 'hyperscript-helpers'
import { Icon } from 'react-fa'

export const faIcon = Icon

const elements = hh(h)
export const {
  a,
  div,
  header,
  img,
  input,
  span
} = elements
export default elements
