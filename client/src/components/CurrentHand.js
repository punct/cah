import { createElement } from 'react'
import { connect } from 'react-redux'
import { div } from './utils'
import Card from './Card'
import { selectHandCard } from '../actions'

function mapStateToProps (state) {
  return {
    cards: state.currentHand
  }
}

function mapDispatchToProps (dispatch) {
  return {
    onSelectHandCard: id => {
      return dispatch(selectHandCard({ id }))
    }
  }
}

function CurrentHand (props) {
  return (
    div({ className: 'current-hand' },
      props.cards.map(card => createElement(Card, {
        card: card,
        isQuestion: false,
        onSelect: props.onSelectHandCard
      }))
    )
  )
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CurrentHand)
