import { createElement } from 'react'
import { connect } from 'react-redux'
import { div, img } from './utils'
import { startGame } from '../actions'
import Login from './Login'

function renderPartyMember (props) {
  const { avatar, displayName } = props
  return (
    div({ className: 'current-party-member' }, [
      img({ className: 'current-party-member-avatar', src: avatar, alt: displayName }),
      div({ className: 'current-party-member-name' }, displayName)
    ])
  )
}

function Party (props) {
  const { party } = props
  return (
    div({ className: 'current-party' }, [
      div({ className: 'current-party-label label' }, 'Current party'),
      div({ className: 'current-party-members' },
        party.map(player => renderPartyMember(player))
      ),
      div({ className: 'start-game' }, 'Start game')
    ])
  )
}

function Lobby (props) {
  const { currentUser, party, onStartGame } = props
  const startGame = event => onStartGame()

  return (
    div({ className: 'lobby' }, [
      div({ className: 'lobby-header' }, 'Cards Against Humanity'),
      (!currentUser
        ? createElement(Login)
        : createElement(Party, {
          party,
          startGame
        })
      ),
      div({ className: 'lobby-footer' }, 'version: 0.0.1')
    ])
  )
}

function mapStateToProps (state) {
  return {
    currentUser: state.currentUser,
    party: state.party
  }
}

function mapDispatchToProps (dispatch) {
  return {
    onStartGame: id => dispatch(startGame())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Lobby)
