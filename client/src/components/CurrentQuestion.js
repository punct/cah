import { createElement } from 'react'
import { connect } from 'react-redux'
import { div, span } from './utils'
import Card from './Card'
import { submitAnswers } from '../actions'

function mapStateToProps (state) {
  return {
    card: state.currentQuestion,
    selected: state.currentHand.filter(card => card.selected).length,
    desired: state.currentQuestion.pick
  }
}

function mapDispatchToProps (dispatch) {
  return {
    onAnswersSubmit: () => dispatch(submitAnswers())
  }
}

function AnswerSubmitter (props) {
  const { selected, desired } = props

  const isValid = `submit-answer-${(selected === desired) ? 'valid' : 'invalid'}`
  const selectedStatus = `${selected}/${desired} selected`
  const onSubmit = () => props.onAnswersSubmit()

  return (
    div({ className: `submit-answer ${isValid}`, onClick: onSubmit }, [
      div({ className: 'submit-answer-label' }, 'Submit answers'),
      div({ className: 'selected-status' }, selectedStatus)
    ])
  )
}

function CurrentQuestion (props) {
  return (
    div({ className: 'current-question' }, [
      span({ className: 'current-question-title label' }, 'Current question'),
      createElement(Card, {
        card: props.card,
        isQuestion: true
      }),
      createElement(AnswerSubmitter, {
        selected: props.selected,
        desired: props.desired,
        onAnswersSubmit: props.onAnswersSubmit
      })
    ])
  )
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CurrentQuestion)
