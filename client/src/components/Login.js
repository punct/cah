import { Component } from 'react'
import { connect } from 'react-redux'
import { div, input } from './utils'
import { loginSubmit } from '../actions'

class Login extends Component {
  constructor (props) {
    super(props)

    this.state = {
      nickname: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleKeyDown = this.handleKeyDown.bind(this)
  }

  handleChange (event) {
    this.setState({ nickname: event.target.value })
  }

  handleKeyDown (event) {
    if (event.key.toUpperCase() === 'ESCAPE') {
      return this.setState({ nickname: '' })
    }
    if (event.key.toUpperCase() === 'ENTER') {
      return this.props.onSubmit(this.state.nickname)
    }
  }

  render () {
    return (
      div({ className: 'login' }, [
        div({ className: 'login-label label' }, 'Choose a nickname:'),
        input({
          className: 'login-input',
          value: this.state.nickname,
          onInput: this.handleChange,
          onKeyDown: this.handleKeyDown,
          autoFocus: true
        })
      ])
    )
  }
}

function mapStateToProps (state) {
  return {}
}

function mapDispatchToProps (dispatch) {
  return {
    // onInputChange: event => dispatch(loginInputChange({ nickname: event.target.value })),
    // onInputClear: () => dispatch(loginInputChange({ nickname: '' })),
    onSubmit: () => dispatch(loginSubmit())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)
