import { createElement } from 'react'
import { div, span, faIcon } from './utils'

function Card (props) {
  const { card, isQuestion, onSelect } = props

  const cardType = (isQuestion) ? 'card-question' : 'card-answer'
  const cardSelected = (card.selected) ? 'card-selected' : ''
  const onCardSelect = event => onSelect(card.id)

  return (
    div({ className: `card ${cardType} ${cardSelected}`, onClick: onCardSelect }, [
      span({}, card.label),
      (card.selected)
        ? createElement(faIcon, { className: 'checkbox', name: 'check' })
        : ''
    ])
  )
}

export default Card
