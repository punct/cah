export const LOGIN_INPUT_CHANGE = 'LOGIN_INPUT_CHANGE'
export const LOGIN_SUBMIT = 'LOGIN_SUBMIT'

export const SELECT_HAND_CARD = 'SELECT_HAND_CARD'
export const SUBMIT_ANSWERS = 'SUBMIT_ANSWERS'
export const START_GAME = 'START_GAME'

export function loginInputChange ({ nickname }) {
  return {
    type: LOGIN_INPUT_CHANGE,
    payload: nickname
  }
}

export function loginSubmit () {
  return {
    type: LOGIN_SUBMIT
  }
}

export function selectHandCard (card) {
  return {
    type: SELECT_HAND_CARD,
    payload: card
  }
}

export function submitAnswers () {
  return {
    type: SUBMIT_ANSWERS
  }
}

export function startGame () {
  return {
    type: START_GAME
  }
}
