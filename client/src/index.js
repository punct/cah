import { createElement } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { store } from './store'

import App from './components/App'

import './index.css'

ReactDOM.render(
  createElement(Provider, { store },
    createElement(App)
  ),
  document.getElementById('root')
)
